/// <reference path="./steps.d.ts" />
Feature('Avangate test');

Scenario('The email exists', (I) => {
    I.amOnPage('/order/checkout.php?PRODS=4628572&SHORT_FORM=1&CART=1&CARD=1&ORDERSTYLE=nLWs5JapnH4%3D&__c=1');
    I.see('email');
});


Scenario('The user is able to type a valid email address', (I) => {
    I.amOnPage('/order/checkout.php?PRODS=4628572&SHORT_FORM=1&CART=1&CARD=1&ORDERSTYLE=nLWs5JapnH4%3D&__c=1');
    I.fillField('email', 'ionut.anghel@rocketmail.com');
    I.wait(2)
});


Scenario('The email field turns red if address is invalid(doesn_t contain @)', (I) => {
    I.amOnPage('/order/checkout.php?PRODS=4628572&SHORT_FORM=1&CART=1&CARD=1&ORDERSTYLE=nLWs5JapnH4%3D&__c=1');
    I.fillField('email', 'ionut.anghel.rocketmail.com');
    I.wait(2);
    I.click('//*[@id="nameoncard"]');
    I.seeCssPropertiesOnElements('//*[@id="email"]', { 'background-color': "#ffe1e1" });
    I.wait(4)
});


Scenario('The price modifies after quantity is modified', (I) => {
    I.amOnPage('/order/checkout.php?PRODS=4628572&SHORT_FORM=1&CART=1&CARD=1&ORDERSTYLE=nLWs5JapnH4%3D&__c=1');
    I.click('#order__listing__row__4628572 > td.order__listing__item__name > div > span > a');
    I.wait(4);
    I.click('//*[@id="order__product__pricingoption__4628572_2750"]');
    I.wait(3);
    I.click('//*[@id="order__product__pricingoption__4628572_2750"]/option[2]');
    I.wait(3);
    I.seeInSource('<span id="base_product_price_4628572">162,60</span>');
    I.wait(3)
});


Scenario('Add to cart product -> goes to shopping cart', (I) => {
    I.amOnPage('/order/checkout.php?PRODS=4628572&SHORT_FORM=1&CART=1&CARD=1&ORDERSTYLE=nLWs5JapnH4%3D&__c=1');
    I.click('//*[@id="order__crossselling"]/table/tbody/tr[4]/td/input[2]')
    I.wait(5)
    I.seeElement('//*[@id="order__listing__row__4689628"]/td[3]/table')
});
